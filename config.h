/* See LICENSE file for copyright and license details. */

/* appearance */
static unsigned int borderpx = 1;	/* border pixel of windows */
static unsigned int snap = 32;	/* snap pixel */
static int swallowfloating = 0;	/* 1 means swallow floating windows by default */
static int showbar = 1;		/* 0 means no bar */
static int topbar = 1;		/* 0 means bottom bar */
static const char *fonts[] = { "monospace:size=12", "Hack Nerd Font:size=8:antialias=false" };
static const char dmenufont[] = "monospace:size=12";
static char normbgcolor[] = "#222222";
static char normbordercolor[] = "#444444";
static char normfgcolor[] = "#bbbbbb";
static char selfgcolor[] = "#eeeeee";
static char selbordercolor[] = "#005577";
static char selbgcolor[] = "#005577";
static char *colors[][3] = {
  /*               fg         bg         border   */
  [SchemeNorm] = {normfgcolor, normbgcolor, normbordercolor},
  [SchemeSel] = {selfgcolor, selbgcolor, selbordercolor},
};

// scratchpads

typedef struct
{
  const char *name;
  const void *cmd;
} Sp;
const char *spcmd1[] = { "st", "-n", "spterm", "-g", "120x34", NULL };
const char *spcmd2[] =
  { "st", "-n", "spcalc", "-g", "50x20", "-e", "qalc", NULL };
const char *spcmd3[] =
  { "st", "-n", "spnvim", "-g", "120x34", "-e", "nvim", NULL };
static Sp scratchpads[] = {
  /* name         cmd */
  {"spterm", spcmd1},
  {"spcalc", spcmd2},
  {"spnvim", spcmd3},
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
  /* xprop(1):
   *      WM_CLASS(STRING) = instance, class
   *      WM_NAME(STRING) = title
   */
  /* class                instance        title           tags mask       isfloating      isterminal      noswallow       monitor */
  {"Tor Browser", NULL, NULL, 0, 1, 0, 0, -1},
  {"St", NULL, NULL, 0, 0, 1, 0, -1},
  {NULL, NULL, "Event Tester", 0, 0, 0, 1, -1},	// xev
  {NULL, "spterm", NULL, SPTAG (0), 1, 1, 0, -1},
  {NULL, "spcalc", NULL, SPTAG (1), 1, 1, 0, -1},
  {NULL, "spnvim", NULL, SPTAG (2), 1, 1, 0, -1},
};

/* layout(s) */
static float mfact = 0.55;	/* factor of master area size [0.05..0.95] */
static int nmaster = 1;		/* number of clients in master area */
static int resizehints = 0;	/* 1 means respect size hints in tiled resizals */

#include "fibonacci.c"
static const Layout layouts[] = {
  /* symbol     arrange function */
  {"[@]", spiral},		/* first entry is default */
  {"[\\]", dwindle},

  {"[]=", tile},
  {"[M]", monocle},

  {"><>", NULL},		/* no layout function means floating behavior */
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,			KEY,		view,		{.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,		KEY,		toggleview,	{.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,		KEY,		tag,		{.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask,	KEY,		toggletag,	{.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

#define STATUSBAR "dwmblocks"

/* commands */
static char dmenumon[2] = "0";	/* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] =
  { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", normbgcolor, "-nf",
  normfgcolor, "-sb", selbordercolor, "-sf", selfgcolor, NULL
};
static const char *termcmd[] = { "st", NULL };

/* Xresources prefs */

ResourcePref resources[] = {
  {".color0", STRING, &normbgcolor},
  {".color0", STRING, &normbordercolor},
  {".color7", STRING, &normfgcolor},
  {".color8", STRING, &selbgcolor},
  {".color8", STRING, &selbordercolor},
  {".color15", STRING, &selfgcolor},
  {"borderpx", INTEGER, &borderpx},
  {"snap", INTEGER, &snap},
  {"showbar", INTEGER, &showbar},
  {"topbar", INTEGER, &topbar},
  {"nmaster", INTEGER, &nmaster},
  {"resizehints", INTEGER, &resizehints},
  {"mfact", FLOAT, &mfact},
};

static Key keys[] = {
  /* modifier			key             function        argument */

  /* Window Manager Keybinds */
  {MODKEY, 			XK_d,			spawn,		{.v = dmenucmd}},
  {MODKEY,			XK_Return,		spawn,		{.v = termcmd}},
  {MODKEY|ShiftMask,		XK_Return,		togglescratch,	{.ui = 0}},
  {MODKEY,			XK_b,			togglebar,	{0}},
  {MODKEY,			XK_j,			focusstack,	{.i = +1}},
  {MODKEY,			XK_k,			focusstack,	{.i = -1}},
  {MODKEY,			XK_o,			incnmaster,	{.i = +1}},
  {MODKEY|ShiftMask,		XK_o,			incnmaster,	{.i = -1}},
  {MODKEY,			XK_h,			setmfact,	{.f = -0.05}},
  {MODKEY,			XK_l,			setmfact,	{.f = +0.05}},
  {MODKEY,			XK_semicolon,		togglescratch,	{.ui = 1}},
  {MODKEY,			XK_apostrophe,		togglescratch,	{.ui = 2}},
  {MODKEY,			XK_space,		zoom,		{0}},
  {MODKEY,			XK_Tab,			view,		{0}},
  {MODKEY,			XK_q,			killclient,	{0}},
  {MODKEY,			XK_t,			setlayout,	{.v = &layouts[2]}},
  {MODKEY,			XK_y,			setlayout,	{.v = &layouts[0]}},
  {MODKEY|ShiftMask,		XK_y,			setlayout,	{.v = &layouts[1]}},
  {MODKEY,			XK_u,			setlayout,	{.v = &layouts[3]}},
  {MODKEY,			XK_f,			togglefullscr,	{0}},
  {MODKEY|ShiftMask,		XK_f,			setlayout,	{.v = &layouts[4]}},
  {MODKEY|ShiftMask,		XK_space,		togglefloating, {0}},
  {MODKEY,			XK_0,			view,		{.ui = ~0}},
  {MODKEY|ShiftMask,		XK_0,			tag, 		{.ui = ~0}},
  TAGKEYS (XK_1, 0)
    TAGKEYS (XK_2, 1)
    TAGKEYS (XK_3, 2)
    TAGKEYS (XK_4, 3)
    TAGKEYS (XK_5, 4)
    TAGKEYS (XK_6, 5)
    TAGKEYS (XK_7, 6)
    TAGKEYS (XK_8, 7)
    TAGKEYS (XK_9, 8)
  {MODKEY|ControlMask, 			XK_h,		focusmon,	{.i = -1},},
  {MODKEY|ControlMask,			XK_l,		focusmon,	{.i = +1},},
  {MODKEY|ControlMask|ShiftMask,	XK_h,		tagmon,		{.i = -1},},
  {MODKEY|ControlMask|ShiftMask,	XK_l,		tagmon,		{.i = +1},},

  /* Programs */
  {MODKEY,			XK_w,			spawn,		SHCMD("$BROWSER")},
  {MODKEY|ShiftMask,		XK_w,			spawn,		SHCMD("torbrowser-launcher")},
  {MODKEY,			XK_c,			spawn,		SHCMD("quickmedia matrix --use-system-mpv-config"),},
  {MODKEY,			XK_e,			spawn,		SHCMD("st -e neomutt")},
  {MODKEY,			XK_m,			spawn,		SHCMD("st -e ncmpcpp")},
  {MODKEY,			XK_F4,			spawn,		SHCMD("st -e pulsemixer; kill -44 $(pidof dwmblocks)")},
  {MODKEY|ShiftMask,		XK_c,			spawn,		SHCMD("st -e calcurse")},
  {MODKEY,			XK_n,			spawn,		SHCMD("st -e $EDITOR -c VimwikiIndex")},
  {MODKEY|ShiftMask,		XK_n,			spawn,		SHCMD("st -e newsboat")},
  {MODKEY,			XK_F6,			spawn,		SHCMD("st -e tremc")},
  {MODKEY,			XK_r,			spawn,		SHCMD("st -e lf")},
  {MODKEY|ShiftMask,		XK_r,			spawn,		SHCMD("st -e htop")},

  /* Dmenu prompts */
  {MODKEY|ShiftMask,		XK_q,			spawn,		SHCMD("sysact")},
  {MODKEY,			XK_F2,			spawn,		SHCMD("displayselect")},
  {MODKEY,			XK_x,			spawn,		SHCMD("passmenu")},

  /* Volume */
  {MODKEY|ShiftMask,		XK_m,			spawn,		SHCMD("pamixer -t; kill -44 $(pidof dwmblocks)")},
  {MODKEY|ControlMask,		XK_m,			spawn,		SHCMD("pamixer --source 1 -t")},
  {MODKEY,			XK_equal,		spawn,		SHCMD("pamixer -i 5; kill -44 $(pidof dwmblocks)")},
  {MODKEY,			XK_minus,		spawn,		SHCMD("pamixer -d 5; kill -44 $(pidof dwmblocks)")},
  /* MPD */
  {MODKEY|ShiftMask,		XK_comma,		spawn,		SHCMD("mpc prev")},
  {MODKEY|ShiftMask,		XK_period,		spawn,		SHCMD("mpc next")},
  {MODKEY,			XK_comma,		spawn,		SHCMD("mpc seek -10")},
  {MODKEY,			XK_period,		spawn,		SHCMD("mpc seek +10")},
  {MODKEY,			XK_p,			spawn,		SHCMD("mpc toggle")},
  {MODKEY,			XK_bracketright,	spawn,		SHCMD("mpc volume +5")},
  {MODKEY,			XK_bracketleft,		spawn,		SHCMD("mpc volume -5")},

  /* Screenshots */
  {0,				XK_Print,		spawn,		SHCMD("maim pic-full-$(date '+%y%m%d-%H%M-%S').png")},
  {ShiftMask,			XK_Print,		spawn,		SHCMD("maimpick")},
  {MODKEY,			XK_Print,		spawn,		SHCMD("dmenurecord")},
  {MODKEY|ShiftMask,		XK_Print,		spawn,		SHCMD("dmenurecord kill")},
  {MODKEY,			XK_Delete,		spawn,		SHCMD("dmenurecord kill")},
  {MODKEY,			XK_Scroll_Lock,		spawn,		SHCMD("killall screenkey || screenkey &")},

  /* Others */
  {MODKEY|ShiftMask,		XK_p,			spawn,		SHCMD("pauseallmpv")},
  {MODKEY,			XK_grave,		spawn,		SHCMD("dmenuunicode")},
  {MODKEY|ShiftMask,		XK_grave,		spawn,		SHCMD("xkill")},
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
  /* click		event mask      button          function        argument */
  {ClkLtSymbol,		0,		Button1,	setlayout,	{0}},
  {ClkLtSymbol,		0,		Button3,	setlayout,	{.v = &layouts[2]}},
  {ClkWinTitle,		0,		Button2,	zoom,		{0}},
  {ClkClientWin,	MODKEY,		Button1,	movemouse,	{0}},
  {ClkStatusText,	0,		Button1,	sigstatusbar,	{.i = 1}},
  {ClkStatusText,	0,		Button2,	sigstatusbar,	{.i = 2}},
  {ClkStatusText,	0,		Button3,	sigstatusbar,	{.i = 3}},
  {ClkClientWin,	MODKEY,		Button2,	togglefloating,	{0}},
  {ClkClientWin,	MODKEY,		Button3,	resizemouse,	{0}},
  {ClkTagBar,		0,		Button1,	view,		{0}},
  {ClkTagBar,		0,		Button3,	toggleview,	{0}},
  {ClkTagBar,		MODKEY,		Button1,	tag,		{0}},
  {ClkTagBar,		MODKEY,		Button3,	toggletag,	{0}},
};
